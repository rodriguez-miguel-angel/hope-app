import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Location } from "../models/location"; 

let token: string = sessionStorage.getItem("token") || "";

const httpOptions = {
  headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
}

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  locationsURL: string = environment.baseUrl +"/locations";

  constructor(private http: HttpClient) { }

    // get method
    getAllLocations(): Observable<Location[]> {
      return this.http.get<Location[]>(this.locationsURL, httpOptions);
    }

    // get method
    getLocationById(id:number): Observable<Location> {
      const url = `${this.locationsURL}/${id}`;
      return this.http.get<Location>(url, httpOptions);
    }




    // update method
    updateLocation(location:Location): Observable<any> {
      return this.http.put<Location>(this.locationsURL, location, httpOptions);
    }

    // delete method
    deleteLocation(id:number): Observable<Location> {
      const url = `${this.locationsURL}/${id}`;
      return this.http.delete<Location>(url, httpOptions);
    }

    // post method
    addLocation(location:Location): Observable<Location> {
      return this.http.post<Location>(this.locationsURL, location, httpOptions);
    }

}
