import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProviderData } from '../models/provider-data';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization':'admin-auth-token'
  })
}


@Injectable({
  providedIn: 'root'
})


export class ProviderRegistrationService {

  providerRegistrationURL = environment.baseUrl + "/providers";


  constructor( private http: HttpClient) { }

  // get method
  getAllProviders(): Observable<ProviderData[]> {
    console.log("provider service");
    return this.http.get<ProviderData[]>(this.providerRegistrationURL, httpOptions);
  }

  // get method
  getProviderById(id:number): Observable<ProviderData> {
    console.log("provider service");
    const url = `${this.providerRegistrationURL}/${id}`;
    return this.http.get<ProviderData>(url, httpOptions);
  }

  // post method
  registerProvider(providerData:ProviderData): Observable<ProviderData> {
    console.log("provider registration service");
    return this.http.post<ProviderData>(this.providerRegistrationURL, providerData);
  }



}
