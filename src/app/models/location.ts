import { ProviderData } from '../models/provider-data';

export class Location {

    id: number;
    onsite: boolean;
    siteName: string;
    address: string;
    city: string;
    state: string;
    postalCode: string;
    contactPerson: string;
    contactPersonPhoneNumber: string;
    provider: ProviderData;
    provider_id: number;

    constructor(_id?:number, _onsite?:boolean, _siteName?:string, _address?:string, _city?:string, _state?:string, 
        _postalCode?:string, _contactPerson?:string, _contactPersonPhoneNumber?:string, _provider?:ProviderData, _provider_id?:number) {
        
        this.id = _id || 0;
        this.onsite = _onsite || true;  //by default, vaccinations are held at main site
        this.siteName = _siteName || "";
        this.address = _address || "";
        this.city = _city || "";
        this.state = _state || "";
        this.postalCode = _postalCode || "";
        this.contactPerson = _contactPerson || "";
        this.contactPersonPhoneNumber = _contactPersonPhoneNumber || "";
        this.provider = _provider || new ProviderData();
        this.provider_id = _provider_id || 0;
    }


}
