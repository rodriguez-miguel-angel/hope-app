export class VaccinationData {
    
    id: number;
    Date: string;
    ShortName: string;      // ShortName for state
    LongName: string;       // ShortName for state
    Doses_Administered: string;
    Doses_Distributed: string;

    constructor(_id?:number, _Date?:string, _ShortName?:string, _LongName?:string, _Doses_Administered?:string, _Doses_Distributed?:string) {
        this.id = _id || 0;
        this.Date = _Date || "";
        this.ShortName = _ShortName || "";
        this.LongName = _LongName || "";
        this.Doses_Administered = _Doses_Administered || "";
        this.Doses_Distributed = _Doses_Distributed || "";
    }



}
