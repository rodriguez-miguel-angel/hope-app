import { EmailValidator } from "@angular/forms";

export class Patient {
    firstName: string;
    lastName: string;
    patientEmail: string;
    phoneNumber: string;
    zipcode: string;
    dateOfBirth: Date;
    preExistingCondition: boolean;
    maxDistance: number;

    constructor(_firstName?: string, _lastName?: string, _patientEmail?: string, _phoneNumber?: string, _zipcode?: string, _dateOfBirth?: Date, _preExistingCondition?: boolean, _maxDistance?: number) {
        this.firstName = _firstName || "";
        this.lastName = _lastName || "";
        this.patientEmail = _patientEmail || "";
        this.phoneNumber = _phoneNumber || "";
        this.zipcode = _zipcode || "";
        this.dateOfBirth = _dateOfBirth || new Date();
        this.preExistingCondition = _preExistingCondition || false;
        this.maxDistance = _maxDistance || 10;
    }
}