import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
import { LoginComponent } from '../login/login.component';
import { NewsPortalComponent } from '../news-portal/news-portal.component';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  // Later our authService will decide which will be true
  // by default these needs to be set to false
  isLoggedIn = false;
  isProvider = false;
  isPatient = false;

  constructor(public loginModal: MatDialog, private authService: AuthService) { }

  openLoginCard(){
    this.loginModal.open(LoginComponent, {panelClass: "mat-dialog-master"});
  }
  openRegistrationCard(){
    this.loginModal.open(RegisterComponent, {panelClass: "mat-dialog-master"});
  }
  logout(){
    this.authService.logout();
  }

  ngOnInit(): void {
    this.authService.loginStatusObservable.subscribe((isLoggedIn)=>{this.isLoggedIn=isLoggedIn});
    this.authService.isPatientObservable.subscribe((isPatient)=>{this.isPatient=isPatient});
    this.authService.isProviderObservable.subscribe((isProvider)=>{this.isProvider=isProvider});
  }
}
