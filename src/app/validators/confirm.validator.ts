import { AbstractControl, FormControl, ValidatorFn } from '@angular/forms';
    
export function ConfirmedValidator(control: FormControl): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null =>  {
        if(control.value === "testing"){
            return null;
        } else{
            console.log(control.value);
            return {"passwordMatch": false};
        }
    }
}
